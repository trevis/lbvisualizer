﻿# 🚀  LBVisualizer
Use this to visual landblock boundaries, and (un)walkable slopes / deep water.

## Installing
Check out [Releases](https://gitlab.com/trevis/lbvisualizer/-/releases) for the latest version.

## Requirements
- [Decal](https://www.decaldev.com/) >= v2.9.8.3
- [UtilityBelt.Service](https://gitlab.com/utilitybelt/utilitybelt.service/-/releases) (installs automatically with this plugin)