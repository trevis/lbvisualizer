﻿using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Microsoft.DirectX.Direct3D;
using Microsoft.DirectX;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using UtilityBelt.Service;
using UtilityBelt.Service.Lib.ACClientModule;
using UtilityBelt.Service.Lib.Settings;
using UtilityBelt.Service.Views;
using UtilityBelt.Scripting.Interop;
using System.Diagnostics;

namespace LBVisualizer {
    internal class Visualizer : IDisposable {
        private PluginCore _plugin;

        private double time = 0;
        private DateTime Start = DateTime.UtcNow;

        internal Dictionary<uint, LandblockGeometry> LoadedLandblocks = [];
        internal uint CurrentLandblock = 0;
        internal uint CurrentCameraLandblock = 0;

        public ManagedTexture DeepWaterTexture { get; }
        public ManagedTexture LandblockBoundaryTexture { get; }
        public float of { get; private set; }

        public Visualizer(PluginCore pluginCore) {
            _plugin = pluginCore;

            _plugin.Settings.Changed += Settings_Changed;

            TryUpdateLandblockBoundaries();

            CoreManager.Current.RenderFrame += Current_RenderFrame;

            DeepWaterTexture = new ManagedTexture(Path.Combine(PluginCore.AssemblyDirectory, "Resources", "deepwater.png"));
            LandblockBoundaryTexture = new ManagedTexture(Path.Combine(PluginCore.AssemblyDirectory, "Resources", "landblockboundary.png"));
        }

        private void Current_RenderFrame(object sender, EventArgs e) {
            try {
                if (!_plugin.ShowLandblockBoundaries && !_plugin.ShowUnwalkableCells && !_plugin.HighlightCurrentLandblockGround) {
                    return;
                }

                time = (DateTime.UtcNow - Start).TotalSeconds;
                var currentLandblock = ((uint)CoreManager.Current.Actions.Landcell & 0xFFFF0000);
                if (CurrentLandblock != currentLandblock) {
                    TryUpdateLandblockBoundaries();
                }

                using (var stateBlock = new StateBlock(_plugin.D3Ddevice, StateBlockType.All)) {
                    stateBlock.Capture();

                    ResetDirectXState();

                    var lbid = Camera.GetFrame().landblock;
                    var LandblockX = (int)((lbid >> 24) & 0xFF);
                    var LandblockY = (int)((lbid >> 16) & 0xFF);

                    var geometries = LoadedLandblocks.Values.ToList();
                    foreach (var geometry in geometries) {
                        _plugin.D3Ddevice.RenderState.CullMode = Cull.None;
                        var world = Matrix.Identity;
                        world.Translate(new Vector3((geometry.LandblockX - LandblockX) * 192f, _plugin.UnwalkableCellsZOffset, (geometry.LandblockY - LandblockY) * 192f));
                        _plugin.D3Ddevice.Transform.World = world;
                        _plugin.D3Ddevice.SetTexture(0, null);

                        if (_plugin.ShowUnwalkableCells) {
                            if (geometry.UnwalkableVertices is not null) {
                                _plugin.D3Ddevice.VertexFormat = CustomVertex.PositionColored.Format;
                                _plugin.D3Ddevice.DrawUserPrimitives(PrimitiveType.TriangleList, geometry.UnwalkableVertices.Length / 3, geometry.UnwalkableVertices);
                            }

                            if (geometry.IsDeepWater && geometry.WallVertices != null) {
                                var floatOffset = (float)(((int)time % 2 == 0) ? 1 - (time % 1) : (time % 1));
                                world.Translate(new Vector3((geometry.LandblockX - LandblockX) * 192f + 0.08f, _plugin.UnwalkableCellsZOffset - LandblockGeometry.WALL_HEIGHT + 2.5f + geometry.AverageHeight + (floatOffset * -0.1f), (geometry.LandblockY - LandblockY) * 192f + 0.08f));
                                _plugin.D3Ddevice.Transform.World = world;

                                _plugin.D3Ddevice.SetSamplerState(0, SamplerStageStates.AddressV, (int)TextureAddress.Border);
                                _plugin.D3Ddevice.SetTexture(0, DeepWaterTexture.Texture);
                                _plugin.D3Ddevice.VertexFormat = CustomVertex.PositionTextured.Format;
                                _plugin.D3Ddevice.DrawUserPrimitives(PrimitiveType.TriangleList, geometry.WallVertices.Length / 3, geometry.WallVertices);
                            }
                        }

                        if (_plugin.HighlightCurrentLandblockGround && (geometry.Landblock & 0xFFFF0000) == (currentLandblock & 0xFFFF0000) && geometry.WalkableVertices != null) {
                            _plugin.D3Ddevice.VertexFormat = CustomVertex.PositionColored.Format;
                            _plugin.D3Ddevice.DrawUserPrimitives(PrimitiveType.TriangleList, geometry.WalkableVertices.Length / 3, geometry.WalkableVertices);
                        }

                        if (_plugin.ShowLandblockBoundaries) {
                            if (geometry.WallVertices != null) {
                                world.Translate(new Vector3((geometry.LandblockX - LandblockX) * 192f, geometry.LowestHeight - 3f, (geometry.LandblockY - LandblockY) * 192f));
                                _plugin.D3Ddevice.Transform.World = world;
                                _plugin.D3Ddevice.RenderState.CullMode = Cull.CounterClockwise;
                                _plugin.D3Ddevice.SetSamplerState(0, SamplerStageStates.AddressV, (int)TextureAddress.Wrap);

                                _plugin.D3Ddevice.SetTexture(0, LandblockBoundaryTexture.Texture);
                                _plugin.D3Ddevice.VertexFormat = CustomVertex.PositionTextured.Format;
                                _plugin.D3Ddevice.DrawUserPrimitives(PrimitiveType.TriangleList, geometry.WallVertices.Length / 3, geometry.WallVertices);
                            }
                        }
                    }

                    stateBlock.Apply();
                }
            }
            catch (Exception ex) {
                PluginCore.Log(ex);
            }
        }

        private void ResetDirectXState() {
            _plugin.D3Ddevice.Transform.View = Camera.GetD3DViewTransform();
            _plugin.D3Ddevice.Transform.World = Matrix.Identity;

            // thanks paradox for proper dx state resetting
            _plugin.D3Ddevice.RenderState.Lighting = false;
            _plugin.D3Ddevice.RenderState.FogEnable = true;
            /*
            _plugin.D3Ddevice.RenderState.FogStart = 0;
            _plugin.D3Ddevice.RenderState.FogEnd = 120;
            _plugin.D3Ddevice.RenderState.FogColor = Color.FromArgb(255, 0, 20, 20);
            _plugin.D3Ddevice.RenderState.FogDensity = 0.1f;
            */

            _plugin.D3Ddevice.RenderState.ZBufferEnable = true;
            _plugin.D3Ddevice.RenderState.ZBufferWriteEnable = true;
            _plugin.D3Ddevice.RenderState.ZBufferFunction = Compare.LessEqual;

            _plugin.D3Ddevice.RenderState.MultiSampleAntiAlias = true;
            _plugin.D3Ddevice.RenderState.AntiAliasedLineEnable = true;

            _plugin.D3Ddevice.RenderState.CullMode = Cull.None;

            _plugin.D3Ddevice.RenderState.DiffuseMaterialSource = ColorSource.Material;
            _plugin.D3Ddevice.RenderState.AmbientMaterialSource = ColorSource.Material;
            _plugin.D3Ddevice.RenderState.SpecularMaterialSource = ColorSource.Material;
            _plugin.D3Ddevice.RenderState.EmissiveMaterialSource = ColorSource.Material;

            _plugin.D3Ddevice.RenderState.AlphaBlendEnable = true;
            _plugin.D3Ddevice.RenderState.SourceBlend = Blend.SourceAlpha;
            _plugin.D3Ddevice.RenderState.DestinationBlend = Blend.InvSourceAlpha;

            //we use the alpha test to exclude any alpha pixels
            //from drawing (or updating the zbuffer). This forces transparency to be
            //binary rather than allowing actual blending.

            _plugin.D3Ddevice.RenderState.ReferenceAlpha = 1;
            _plugin.D3Ddevice.RenderState.AlphaTestEnable = true;
            _plugin.D3Ddevice.RenderState.AlphaFunction = Compare.GreaterEqual;

            //These are the states specific to untextured objects.
            //_plugin.D3Ddevice.SetTextureStageState(0, TextureStageStates.ColorOperation, false);

            _plugin.D3Ddevice.SetSamplerState(0, SamplerStageStates.AddressU, (int)TextureAddress.Wrap);
            _plugin.D3Ddevice.SetSamplerState(0, SamplerStageStates.AddressV, (int)TextureAddress.Wrap);
            _plugin.D3Ddevice.SetSamplerState(0, SamplerStageStates.BorderColor, 0x00000000);
            _plugin.D3Ddevice.SetTextureStageState(0, TextureStageStates.ColorOperation, 4);
        }

        private void Settings_Changed(object sender, SettingChangedEventArgs e) {
            DisposeLandblockBoundaries();
            TryUpdateLandblockBoundaries();
        }

        private void TryUpdateLandblockBoundaries() {
            if (_plugin.ShowLandblockBoundaries || _plugin.ShowUnwalkableCells || _plugin.HighlightCurrentLandblockGround) {
                DrawLandblockBoundaries();
            }
            else {
                DisposeLandblockBoundaries();
            }
        }

        private void DrawLandblockBoundaries() {
            var currentLandcell = (uint)CoreManager.Current.Actions.Landcell;
            var currentLandblock = currentLandcell & 0xFFFF0000;

            // inside, dont draw
            if ((currentLandcell & 0xFFFF) > 0x100) {
                return;
            }

            // load new landblocks
            var lbX = (int)((currentLandblock >> 24) & 0xFF);
            var lbY = (int)((currentLandblock >> 16) & 0xFF);
            for (var x = -_plugin.LandblockDrawDistance.Value; x <= _plugin.LandblockDrawDistance.Value; x++) {
                for (var y = -_plugin.LandblockDrawDistance.Value; y <= _plugin.LandblockDrawDistance.Value; y++) {
                    if (lbX + x < 0 || lbX + x > 0xFE || lbY + y < 0 || lbY + y > 0xFE) {
                        continue;
                    }

                    var lbId = (uint)((lbX + x) << 24) + (uint)((lbY + y) << 16);
                    if (!LoadedLandblocks.ContainsKey(lbId)) {
                        var geometry = new LandblockGeometry(lbId, _plugin);
                        geometry.Load();
                        LoadedLandblocks.Add(lbId, geometry);
                    }
                }
            }

            // unload far landblocks
            foreach (var lbId in LoadedLandblocks.Keys.ToArray()) {
                var clbX = (int)((lbId >> 24) & 0xFF);
                var clbY = (int)((lbId >> 16) & 0xFF);
                if (Math.Abs(clbX - lbX) > _plugin.LandblockDrawDistance.Value || Math.Abs(clbY - lbY) > _plugin.LandblockDrawDistance.Value) {
                    if (LoadedLandblocks.TryGetValue(lbId, out var toRemove)) {
                        LoadedLandblocks.Remove(lbId);
                        toRemove.Dispose();
                    }
                }
            }
            CurrentLandblock = currentLandblock;
            CurrentCameraLandblock = (Camera.GetFrame().landblock & 0xFFFF0000);
        }

        private void DisposeLandblockBoundaries() {
            CurrentLandblock = 0;
            CurrentCameraLandblock = 0;

            foreach (var kv in LoadedLandblocks) {
                kv.Value.Dispose();
            }

            LoadedLandblocks.Clear();
        }

        public void Dispose() {
            CoreManager.Current.RenderFrame -= Current_RenderFrame;
            _plugin.Settings.Changed -= Settings_Changed;
            DisposeLandblockBoundaries();
            DeepWaterTexture?.Dispose();
            LandblockBoundaryTexture?.Dispose();
        }
    }
}