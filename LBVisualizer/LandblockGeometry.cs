﻿using ACE.DatLoader.FileTypes;
using Decal.Adapter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Service;
using Decal.Adapter.Wrappers;
using Microsoft.DirectX.Direct3D;
using Microsoft.DirectX;
using System.Drawing;
using UtilityBelt.Common.Enums;
using UtilityBelt.Service.Lib.Settings;
using static AcClient.LandDefs;
using System.Runtime.CompilerServices;
using System.IO;
using UtilityBelt.Service.Views;

namespace LBVisualizer {
    public class LandblockGeometry : IDisposable {
        public const float WALL_HEIGHT = 110f;
        private PluginCore _plugin;

        public bool IsDeepWater { get; private set; }
        public float AverageHeight { get; private set; }
        public float LowestHeight { get; private set; }
        public CustomVertex.PositionColored[] UnwalkableVertices { get; private set; }
        public CustomVertex.PositionColored[] WalkableVertices { get; private set; }
        public uint Landblock { get; }
        public bool IsLoaded { get; private set; }
        public uint LandblockX { get; }
        public uint LandblockY { get; }
        public CustomVertex.PositionTextured[] WallVertices { get; private set; }

        public static Vector3[] LandblockVertices;

        public LandblockGeometry(uint landblock, PluginCore plugin) {
            _plugin = plugin;
            Landblock = landblock;
            IsLoaded = false;
            LandblockX = (Landblock >> 24) & 0xFF;
            LandblockY = (Landblock >> 16) & 0xFF;
        }

        public void Load() {
            if ((Landblock & 0x0000FFFF) > 0x100) {
                // inside...
                return;
            }

            // magic from ace
            uint seedA = (uint)(((int)LandblockX << 3) * 214614067);
            uint seedB = (uint)(((int)LandblockX << 3) * 1109124029);

            var landblockData = UBService.CellDat.ReadFromDat<CellLandblock>((Landblock & 0xFFFF0000) | 0x0000FFFF);
            var heightTable = UBService.PortalDat.ReadFromDat<RegionDesc>(318767104u).LandDefs.LandHeightTable;

            if (landblockData.Height == null || landblockData.Height.Count == 0) {
                return;
            }

            AverageHeight = landblockData.Height.Select(h => heightTable[h]).Average();
            LowestHeight = landblockData.Height.Select(h => heightTable[h]).Min();

            if (_plugin.ShowLandblockBoundaries || _plugin.ShowUnwalkableCells) {
                AddWallVertices();
            }

            if (landblockData.Terrain.All(t => !TerrainTypeIsWalkable(t)) || landblockData.Terrain.All(t => TerrainIsWater(t))) {
                // whole landblock is unwalkable...
                IsDeepWater = true;
                //  2    4---3
                //  | \   \  |
                //  |  \   \ |
                //  0---1    5
                var vertices = new Vector3[6];
                vertices[0] = new Vector3(0, heightTable[landblockData.Height[(0 * 9) + 0]], 0);
                vertices[1] = new Vector3(192, heightTable[landblockData.Height[(8 * 9) + 0]], 0);
                vertices[2] = new Vector3(0, heightTable[landblockData.Height[(0 * 9) + 8]], 192);
                vertices[3] = new Vector3(192, heightTable[landblockData.Height[(8 * 9) + 8]], 192);
                vertices[4] = new Vector3(0, heightTable[landblockData.Height[(0 * 9) + 8]], 192);
                vertices[5] = new Vector3(192, heightTable[landblockData.Height[(8 * 9) + 0]], 0);

                UnwalkableVertices = vertices.Select(v => new CustomVertex.PositionColored(v, _plugin.UnwalkableCellsColor.Value)).ToArray();

                return;
            }

            List<Vector3> verticeLookup = new List<Vector3>();
            for (var x = 0; x < 9; x++) {
                for (var y = 0; y < 9; y++) {
                    var z = heightTable[landblockData.Height[((x * 9) + y)]];
                    verticeLookup.Add(new Vector3());
                    verticeLookup[(x * 9) + y] = new Vector3(x * 24f, y * 24f, z);
                }
            }

            var unwalkableVerts = new List<Vector3>();
            var walkableVerts = new List<Vector3>();

            //landcells
            for (uint tileX = 0; tileX < 8; tileX++) {
                for (uint tileY = 0; tileY < 8; tileY++) {
                    var lcid = (Landblock & 0xFFFF0000) + (tileX << 8) + tileY;
                    var magicB = seedB;
                    var magicA = seedA + 1813693831;

                    var splitDir = (uint)(((int)LandblockY << 3) + tileY) * magicA - magicB - 1369149221;

                    int i1 = (int)((tileX * 9) + tileY);
                    int i2 = (int)(((tileX + 1) * 9) + tileY);
                    int i3 = (int)(((tileX) * 9) + (tileY + 1));
                    int i4 = (int)(((tileX + 1) * 9) + tileY + 1);

                    if (splitDir * 2.3283064e-10 < 0.5f) {
                        AddTriVertices(verticeLookup[i1], verticeLookup[i2], verticeLookup[i3], ref unwalkableVerts, ref walkableVerts);
                        AddTriVertices(verticeLookup[i4], verticeLookup[i3], verticeLookup[i2], ref unwalkableVerts, ref walkableVerts);
                    }
                    else {
                        AddTriVertices(verticeLookup[i1], verticeLookup[i2], verticeLookup[i4], ref unwalkableVerts, ref walkableVerts);
                        AddTriVertices(verticeLookup[i1], verticeLookup[i4], verticeLookup[i3], ref unwalkableVerts, ref walkableVerts);
                    }
                }
                seedA += 214614067;
                seedB += 1109124029;
            }

            if (unwalkableVerts.Count > 0) {
                UnwalkableVertices = unwalkableVerts.Select(v => new CustomVertex.PositionColored(new Vector3(v.X, v.Z, v.Y), _plugin.UnwalkableCellsColor.Value)).ToArray();
            }
            if (walkableVerts.Count > 0) {
                WalkableVertices = walkableVerts.Select(v => new CustomVertex.PositionColored(new Vector3(v.X, v.Z, v.Y), _plugin.HighlightCurrentLandblockColor.Value)).ToArray();
            }
        }

        private void AddTriVertices(Vector3 a, Vector3 b, Vector3 c, ref List<Vector3> unwalkableVertices, ref List<Vector3> walkableVertices) {
            if (TriSlopeIsWalkable(a, b, c)) {
                walkableVertices.Add(a);
                walkableVertices.Add(b);
                walkableVertices.Add(c);
            }
            else {
                unwalkableVertices.Add(a);
                unwalkableVertices.Add(b);
                unwalkableVertices.Add(c);
            }
        }

        private void AddWallVertices() {
            //  2    4---3
            //  | \   \  |
            //  |  \   \ |
            //  0---1    5
            var wallVertices = new List<CustomVertex.PositionTextured>();

            var wallHeight = WALL_HEIGHT;

            var rx = 28; // texture repeat x
            var ry = 36; // texture repeat y

            // southern wall
            wallVertices.Add(new CustomVertex.PositionTextured(new Vector3(0, 0, 0), 0, ry));
            wallVertices.Add(new CustomVertex.PositionTextured(new Vector3(192, 0, 0), rx, ry));
            wallVertices.Add(new CustomVertex.PositionTextured(new Vector3(0, 0 + wallHeight, 0), 0, 0));
            wallVertices.Add(new CustomVertex.PositionTextured(new Vector3(192, 0 + wallHeight, 0), rx, 0));
            wallVertices.Add(new CustomVertex.PositionTextured(new Vector3(0, 0 + wallHeight, 0), 0, 0));
            wallVertices.Add(new CustomVertex.PositionTextured(new Vector3(192, 0, 0), rx, ry));

            // northern wall
            wallVertices.Add(new CustomVertex.PositionTextured(new Vector3(192, 0, 192), 0, ry));
            wallVertices.Add(new CustomVertex.PositionTextured(new Vector3(0, 0, 192), rx, ry));
            wallVertices.Add(new CustomVertex.PositionTextured(new Vector3(192, 0 + wallHeight, 192), 0, 0));
            wallVertices.Add(new CustomVertex.PositionTextured(new Vector3(0, 0 + wallHeight, 192), rx, 0));
            wallVertices.Add(new CustomVertex.PositionTextured(new Vector3(192, 0 + wallHeight, 192), 0, 0));
            wallVertices.Add(new CustomVertex.PositionTextured(new Vector3(0, 0, 192), rx, ry));

            // western wall
            wallVertices.Add(new CustomVertex.PositionTextured(new Vector3(0, 0, 192), 0, ry));
            wallVertices.Add(new CustomVertex.PositionTextured(new Vector3(0, 0, 0), rx, ry));
            wallVertices.Add(new CustomVertex.PositionTextured(new Vector3(0, 0 + wallHeight, 192), 0, 0));
            wallVertices.Add(new CustomVertex.PositionTextured(new Vector3(0, 0 + wallHeight, 0), rx, 0));
            wallVertices.Add(new CustomVertex.PositionTextured(new Vector3(0, 0 + wallHeight, 192), 0, 0));
            wallVertices.Add(new CustomVertex.PositionTextured(new Vector3(0, 0, 0), rx, ry));

            // eastern wall
            wallVertices.Add(new CustomVertex.PositionTextured(new Vector3(192, 0, 0), 0, ry));
            wallVertices.Add(new CustomVertex.PositionTextured(new Vector3(192, 0, 192), rx, ry));
            wallVertices.Add(new CustomVertex.PositionTextured(new Vector3(192, 0 + wallHeight, 0), 0, 0));
            wallVertices.Add(new CustomVertex.PositionTextured(new Vector3(192, 0 + wallHeight, 192), rx, 0));
            wallVertices.Add(new CustomVertex.PositionTextured(new Vector3(192, 0 + wallHeight, 0), 0, 0));
            wallVertices.Add(new CustomVertex.PositionTextured(new Vector3(192, 0, 192), rx, ry));

            WallVertices = wallVertices.ToArray();
        }

        private bool TriSlopeIsWalkable(Vector3 a, Vector3 b, Vector3 c) {
            return Vector3.Normalize(Vector3.Cross(a - b, b - c)).Z >= 0.66417414618662751f;
        }


        private bool TerrainTypeIsWalkable(ushort t) {
            TerrainType tType = (TerrainType)((t >> 2) & 0x3F);
            return tType != TerrainType.WaterDeepSea;
        }

        private bool TerrainIsWater(ushort t) {
            TerrainType tType = (TerrainType)((t >> 2) & 0x3F);
            return tType == TerrainType.WaterDeepSea || tType == TerrainType.WaterShallowStillSea || tType == TerrainType.WaterRunning || tType == TerrainType.WaterShallowSea || tType == TerrainType.WaterStandingFresh;
        }

        public void Dispose() {

        }
    }
}
