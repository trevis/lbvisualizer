﻿using Decal.Adapter;
using ImGuiNET;
using System;
using System.Linq;
using System.Numerics;
using UtilityBelt.Service;
using UtilityBelt.Service.Lib.ACClientModule;
using UtilityBelt.Service.Views;

namespace LBVisualizer {
    internal class VisualizerUI : IDisposable {
        private readonly PluginCore _plugin;

        /// <summary>
        /// The UBService Hud
        /// </summary>
        private readonly Hud hud;

        /// <summary>
        /// The default value for TestText.
        /// </summary>
        public const string DefaultTestText = "Some Test Text";

        /// <summary>
        /// Some test text. This value is used to the text input in our UI.
        /// </summary>
        public string TestText = DefaultTestText.ToString();

        public VisualizerUI(PluginCore plugin) {
            _plugin = plugin;

            // Create a new UBService Hud
            hud = UBService.Huds.CreateHud("LBVisualizer");

            // set to show our icon in the UBService HudBar
            hud.ShowInBar = true;
            hud.WindowSettings |= ImGuiWindowFlags.AlwaysAutoResize;

            // subscribe to the hud render event so we can draw some controls
            hud.OnRender += Hud_OnRender;
        }

        /// <summary>
        /// Called every time the ui is redrawing.
        /// </summary>
        private unsafe void Hud_OnRender(object sender, EventArgs e) {
            try {
                ImGui.Text($"Location: {Coordinates.Me}");
                var showLandblockBoundaries = _plugin.ShowLandblockBoundaries.Value;
                if (ImGui.Checkbox("Show landblock boundaries", ref showLandblockBoundaries)) {
                    _plugin.ShowLandblockBoundaries.SetValue(showLandblockBoundaries);
                }
                ImGui.SetItemTooltip(_plugin.ShowLandblockBoundaries.Summary);

                var highlightCurrentLandblockGround = _plugin.HighlightCurrentLandblockGround.Value;
                if (ImGui.Checkbox("Highlight current landblock walkable ground", ref highlightCurrentLandblockGround)) {
                    _plugin.HighlightCurrentLandblockGround.SetValue(highlightCurrentLandblockGround);
                }
                ImGui.SetItemTooltip(_plugin.HighlightCurrentLandblockGround.Summary);

                var highlightCurrentLandblockColor = ColToVec4(_plugin.HighlightCurrentLandblockColor.Value);
                if (ImGui.ColorEdit4("Highlight ground color", ref highlightCurrentLandblockColor, ImGuiColorEditFlags.AlphaBar | ImGuiColorEditFlags.AlphaPreviewHalf)) {
                    _plugin.HighlightCurrentLandblockColor.SetValue(Vec4ToCol(highlightCurrentLandblockColor));
                }
                ImGui.SetItemTooltip(_plugin.UnwalkableCellsColor.Summary);

                var showUnwalkableSlopes = _plugin.ShowUnwalkableCells.Value;
                if (ImGui.Checkbox("Show unwalkable slopes", ref showUnwalkableSlopes)) {
                    _plugin.ShowUnwalkableCells.SetValue(showUnwalkableSlopes);
                }
                ImGui.SetItemTooltip(_plugin.ShowUnwalkableCells.Summary);

                var unwalkableCellsColor = ColToVec4(_plugin.UnwalkableCellsColor.Value);
                if (ImGui.ColorEdit4("Unwalkable cell color", ref unwalkableCellsColor, ImGuiColorEditFlags.AlphaBar | ImGuiColorEditFlags.AlphaPreviewHalf)) {
                    _plugin.UnwalkableCellsColor.SetValue(Vec4ToCol(unwalkableCellsColor));
                }
                ImGui.SetItemTooltip(_plugin.UnwalkableCellsColor.Summary);

                var unwalkableCellsZOffset = _plugin.UnwalkableCellsZOffset.Value;
                if (ImGui.InputFloat("Unwalkable cell z offset", ref unwalkableCellsZOffset, 0.01f, 0.1f)) {
                    _plugin.UnwalkableCellsZOffset.SetValue(unwalkableCellsZOffset);
                }
                ImGui.SetItemTooltip(_plugin.UnwalkableCellsZOffset.Summary);

                var landblockBoundaryDrawDistance = _plugin.LandblockDrawDistance.Value;
                if (ImGui.InputInt("Landblock Draw Distance", ref landblockBoundaryDrawDistance, 1, 1, ImGuiInputTextFlags.CharsDecimal)) {
                    _plugin.LandblockDrawDistance.SetValue(landblockBoundaryDrawDistance);
                }
                ImGui.SetItemTooltip(_plugin.LandblockDrawDistance.Summary);
            }
            catch (Exception ex) {
                PluginCore.Log(ex);
            }
        }

        public static Vector4 ColToVec4(int col) {
            var r = (float)((col) & 0xFF) / 255.0f;
            var g = (float)((col >> 8) & 0xFF) / 255.0f;
            var b = (float)((col >> 16) & 0xFF) / 255.0f;
            var a = (float)((col >> 24) & 0xFF) / 255.0f;

            return new Vector4(b, g, r, a);
        }

        public static int Vec4ToCol(Vector4 v4) {
            return ((int)(v4.X * 255.0f) << 16) |
            ((int)(v4.Y * 255.0f) << 8) |
            ((int)(v4.Z * 255.0f)) |
            ((int)(v4.W * 255.0f) << 24);
        }

        public void Dispose() {
            hud.Dispose();
        }
    }
}