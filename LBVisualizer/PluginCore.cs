﻿using Decal.Adapter;
using Microsoft.DirectX.Direct3D;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using UtilityBelt.Service;
using UtilityBelt.Service.Lib.Settings;
using UtilityBelt.Service.Views.SettingsEditor;

namespace LBVisualizer {
    /// <summary>
    /// This is the main plugin class. When your plugin is loaded, Startup() is called, and when it's unloaded Shutdown() is called.
    /// </summary>
    [FriendlyName("LBVisualizer")]
    public class PluginCore : PluginBase {
        private static string _assemblyDirectory = null;
        private VisualizerUI ui;

        internal Visualizer Visualizer { get; set; }
        internal static Guid IID_IDirect3DDevice9 = new Guid("{D0223B96-BF7A-43fd-92BD-A43B0D82B9EB}");
        internal IntPtr unmanagedD3dPtr;
        internal Device D3Ddevice { get; private set; }

        public Settings Settings;

        [Summary("Show landblock boundaries")]
        public Setting<bool> ShowLandblockBoundaries = new(false);

        [Summary("Highlight current landblock walkable ground")]
        public Setting<bool> HighlightCurrentLandblockGround = new(false);

        [Summary("The color to use when highlighting the current landblock walkable ground")]
        public Setting<int> HighlightCurrentLandblockColor = new(Color.FromArgb(50, 100, 0, 100).ToArgb());

        [Summary("Show unwalkable cells")]
        public Setting<bool> ShowUnwalkableCells = new(false);

        [Summary("Unwalkable cell z offset")]
        public Setting<float> UnwalkableCellsZOffset = new(0.12f);

        [Summary("The color to use when drawing unwalkable cells")]
        public Setting<int> UnwalkableCellsColor = new(Color.FromArgb(100, 100, 0, 0).ToArgb());

        [Summary("The distance in landblocks to draw to, setting to 0 only draws current landblock boundaries / slopes")]
        [MinMax(0, 5)]
        public Setting<int> LandblockDrawDistance = new(1);

        /// <summary>
        /// Assembly directory containing the plugin dll
        /// </summary>
        public static string AssemblyDirectory {
            get {
                if (_assemblyDirectory == null) {
                    try {
                        _assemblyDirectory = System.IO.Path.GetDirectoryName(typeof(PluginCore).Assembly.Location);
                    }
                    catch {
                        _assemblyDirectory = Environment.CurrentDirectory;
                    }
                }
                return _assemblyDirectory;
            }
            set {
                _assemblyDirectory = value;
            }
        }

        /// <summary>
        /// Called when your plugin is first loaded.
        /// </summary>
        protected override void Startup() {
            try {
                object a = CoreManager.Current.Decal.Underlying.GetD3DDevice(ref IID_IDirect3DDevice9);
                Marshal.QueryInterface(Marshal.GetIUnknownForObject(a), ref IID_IDirect3DDevice9, out unmanagedD3dPtr);
                D3Ddevice = new Device(unmanagedD3dPtr);

                var settingsPath = System.IO.Path.Combine(AssemblyDirectory, "settings.json");
                Settings = new Settings(this, settingsPath);
                Settings.Load();

                if (CoreManager.Current.CharacterFilter.LoginStatus == 3) {
                    Init();
                }
                else {
                    CoreManager.Current.CharacterFilter.LoginComplete += CharacterFilter_LoginComplete;
                }
            }
            catch (Exception ex) {
                Log(ex);
            }
        }

        private void Init() {
            Visualizer = new Visualizer(this);
            ui = new VisualizerUI(this);
        }

        /// <summary>
        /// CharacterFilter_LoginComplete event handler.
        /// </summary>
        private void CharacterFilter_LoginComplete(object sender, EventArgs e) {
            // it's generally a good idea to use try/catch blocks inside of decal event handlers.
            // throwing an uncaught exception inside one will generally hard crash the client.
            try {
                Init();
            }
            catch (Exception ex) {
                Log(ex);
            }
        }

        /// <summary>
        /// Called when your plugin is unloaded. Either when logging out, closing the client, or hot reloading.
        /// </summary>
        protected override void Shutdown() {
            try {
                // make sure to unsubscribe from any events we were subscribed to. Not doing so
                // can cause the old plugin to stay loaded between hot reloads.
                CoreManager.Current.CharacterFilter.LoginComplete -= CharacterFilter_LoginComplete;

                ui?.Dispose();
                Visualizer?.Dispose();

                if (Settings != null) {
                    if (Settings.NeedsSave) {
                        Settings.Save();
                    }
                }
            }
            catch (Exception ex) {
                Log(ex);
            }
        }

        #region logging
        /// <summary>
        /// Log an exception to log.txt in the same directory as the plugin.
        /// </summary>
        /// <param name="ex"></param>
        internal static void Log(Exception ex) {
            Log(ex.ToString());
        }

        /// <summary>
        /// Log a string to log.txt in the same directory as the plugin.
        /// </summary>
        /// <param name="message"></param>
        internal static void Log(string message) {
            try {
                File.AppendAllText(System.IO.Path.Combine(AssemblyDirectory, "log.txt"), $"{message}\n");

                CoreManager.Current.Actions.AddChatText(message, 1);
            }
            catch { }
        }
        #endregion // logging
    }
}
